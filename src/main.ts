import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import 'virtual:svg-icons-register'
import 'element-plus/dist/index.css'
// 引入基础的样式
import '@/styles/index.scss'
//@ts-ignore忽略当前文件ts类型的检测否则有红色提示(打包会失败)
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
// 引入路由
import router from './router';
// 引入仓库
import pinia from "./store"
// 引入路由鉴权文件
//@ts-ignore
import "./permisstion"
// 暗黑模式的切换
import 'element-plus/theme-chalk/dark/css-vars.css'
const app = createApp(App)
app.use(ElementPlus, {
    locale: zhCn
})

// 注册全局组件
import gloablComponent from '@/components';
app.use(gloablComponent);


// 使用路由
app.use(router)
// 使用仓库
app.use(pinia)

// 挂载应用实例
app.mount('#app')
