// 此文件用来统一管理项目用户有关的接口
// 例如登录接口，用户信息接口
import request from "@/utils/request";
import type { loginFormData, loginResponseData, userInfoReponseData } from "./type";
enum API {
    // 登录接口
    LOGIN_URL = '/admin/acl/index/login',
    // 获取用户信息
    USERINFO_URL = '/admin/acl/index/info',
    // 退出登录
    LOGOUT_URL = '/admin/acl/index/logout',
}

// 往外暴露请求函数

//登录接口
export const reqLogin = (data: loginFormData) =>
    request.post<any, loginResponseData>(API.LOGIN_URL, data)
//获取用户信息
export const reqUserInfo = () =>
    request.get<any, userInfoReponseData>(API.USERINFO_URL)
//退出登录
export const reqLogout = () => request.post<any, any>(API.LOGOUT_URL)