//角色管理模块的的接口
import request from '@/utils/request'
import type { RoleResponseData, RoleData, MenuResponseData } from './type'

//枚举地址
enum API {
    //获取全部的职位接口
    ALLROLE_URL = '/admin/acl/role/',
    //新增岗位的接口地址
    ADDROLE_URL = '/admin/acl/role/save',
    //更新已有的职位
    UPDATEROLE_URL = '/admin/acl/role/update',
    //获取全部的菜单与按钮的数据
    ALLPERMISSTION = '/admin/acl/permission/toAssign/',
    //给相应的职位分配权限
    SETPERMISTION_URL = '/admin/acl/permission/doAssign/?',
    //删除已有的职位
    REMOVEROLE_URL = '/admin/acl/role/remove/',
}

// 获取全部职位的方法
export const reqAllRole = (page: number, limit: number, roleName: string) => {
    return request.get<RoleResponseData>(API.ALLROLE_URL + `${page}/${limit}?roleName=${roleName}`)
}

// 更新或添加职位的方法
export const reqAddOrUpdateRole = (data: RoleData) => {
    if (data.id) {
        return request.put<any, RoleResponseData>(API.UPDATEROLE_URL, data)
    } else {
        return request.post<any, RoleResponseData>(API.ADDROLE_URL, data)
    }
}
// 获取全部权限及按钮权限的方法
export const reqAllPermission = (roleId: number) => {
    return request.get<any, MenuResponseData>(API.ALLPERMISSTION + roleId)
}
//给相应的职位下发权限
export const reqSetPermisstion = (roleId: number, permissionId: number[]) =>
    request.post(
        API.SETPERMISTION_URL + `roleId=${roleId}&permissionId=${permissionId}`,
    )
//删除已有的职位
export const reqRemoveRole = (roleId: number) =>
    request.delete<any, any>(API.REMOVEROLE_URL + roleId)