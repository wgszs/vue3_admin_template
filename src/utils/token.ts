// 将存入本地存储token的方法进行封装
// 储存token
export const SET_TOKEN = (token:string) =>{
    localStorage.setItem("token",token)
}

// 获取token
export const GET_TOKEN = () =>{
    return localStorage.getItem("token")
}

// 删除本地数据的方法
export const REMOVE_TOKEN = () =>{
    localStorage.removeItem("token")
}