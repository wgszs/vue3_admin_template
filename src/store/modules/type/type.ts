import { CategoryObj } from "@/api/product/attr/type"
import { RouteRecordRaw } from "vue-router"

// 小仓库state的数据类型
export interface userState {
    token: null | String
    menuRoutes: RouteRecordRaw[],
    username: String,
    avatar: String
}

//定义分类仓库state对象的ts类型
export interface CategoryState {
    c1Id: string | number
    c1Arr: CategoryObj[]
    c2Arr: CategoryObj[]
    c2Id: string | number
    c3Arr: CategoryObj[]
    c3Id: string | number
}
