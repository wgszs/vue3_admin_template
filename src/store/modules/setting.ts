import { defineStore } from "pinia"

const useLayOutSettingStore = defineStore('SettingStore',{
    state: () => {
        return {
            fold:false,      // 控制菜单是否折叠,
            refsh:false,      // 控制菜单是否刷新
        }
    }
})

export default useLayOutSettingStore