import { defineStore } from "pinia"
// 引入登录的方法
import { reqLogin, reqUserInfo, reqLogout } from "@/api/user"
// 引入数据类型
import type {
    loginFormData,
    loginResponseData,
    userInfoReponseData,
} from '@/api/user/type'
import type { userState } from "./type/type"
//引入操作本地存储的工具方法
import { SET_TOKEN, GET_TOKEN, REMOVE_TOKEN } from '@/utils/token'
//引入路由(常量路由)
import { constantRoute, asnycRoute, anyRoute } from '@/router/routes'
//引入深拷贝方法
import cloneDeep from 'lodash/cloneDeep'
import router from '@/router'
//用于过滤当前用户需要展示的异步路由
function filterAsyncRoute(asnycRoute: any, routes: any) {
    return asnycRoute.filter((item: any) => {
        if (routes.includes(item.name)) {
            if (item.children && item.children.length > 0) {
                //硅谷333账号:product\trademark\attr\sku
                item.children = filterAsyncRoute(item.children, routes)
            }
            return true
        }
    })
}
const useUserStore:any = defineStore("User", {
    state: (): userState => {
        // 存放数据的地方
        return {
            token: GET_TOKEN(), // 用户的唯一标识token
            menuRoutes: constantRoute, // 用户的路由表
            username: '',
            avatar: '',
        }
    },
    // 处理异步 | 逻辑的地方
    actions: {
        // 用户登录的方法
        async userLogin(data: loginFormData) {
            const result: loginResponseData = await reqLogin(data)
            // 登录成功 获取token 存放token
            if (result.code === 200) {
                this.token = (result.data as string)
                // 本地存储一份token
                SET_TOKEN((result.data as string))
                // 返回一个成功的标志
                return "ok"
            } else {
                // 登陆失败
                return Promise.reject(new Error(result.data))
            }

        },
        // 获取用户信息的方法
        async userInfo() {
            let result: userInfoReponseData = await reqUserInfo()

            if (result.code === 200) {
                this.username = result.data.name
                this.avatar = result.data.avatar
                //计算当前用户需要展示的异步路由
                const userAsyncRoute = filterAsyncRoute(
                    cloneDeep(asnycRoute),
                    result.data.routes,
                )
                //菜单需要的数据整理完毕
                this.menuRoutes = [...constantRoute, ...userAsyncRoute, anyRoute]
                    //目前路由器管理的只有常量路由:用户计算完毕异步路由、任意路由动态追加
                    ;[...userAsyncRoute, anyRoute].forEach((route: any) => {
                        router.addRoute(route)
                    })
                return "ok"
            } else {
                return Promise.reject(new Error(result.message))
            }
        },
        // 用户退出登录的方法
        async userlogout() {
            let result: any = await reqLogout()
            if (result.code === 200) {
                this.token = ''
                this.username = ''
                this.avatar = ''
                // 清除本地存储的token
                REMOVE_TOKEN()
                return "ok"
            } else {
                return Promise.reject(new Error(result.message))
            }
        }


    },
    getters: {

    }
})

export default useUserStore