import { CategoryResponseData } from './../../api/product/attr/type';
import { defineStore } from "pinia"
import { reqC1, reqC2, reqC3 } from "@/api/product/attr"
import type { CategoryState } from "./type/type"
let useCategoryStore = defineStore('category', {
    state: (): CategoryState => {
        return {
            c1Arr: [],
            // 获取一级分类数据的id
            c1Id: '',
            // 获取二级分类数据的id
            c2Arr: [],
            c2Id: '',
            // 获取三级分类数据的id
            c3Arr: [],
            c3Id: ''
        }
    },
    actions: {
        // 获取一级分类数据
        async getC1() {
            let result: CategoryResponseData = await reqC1()
            if (result.code === 200) {
                this.c1Arr = result.data
            }
        },
        // 获取二级分类数据
        async getC2() {
            let result: CategoryResponseData = await reqC2(this.c1Id)
            if (result.code === 200) {
                this.c2Arr = result.data
            }
        },
        // 获取三级分类数据
        async getC3() {
            let result: CategoryResponseData = await reqC3(this.c2Id)
            if (result.code === 200) {
                this.c3Arr = result.data
            }
        }

    },
    getters: {

    }
})

export default useCategoryStore